# Introduction

Soundscape research demands a holistic approach for the analysis of environments, yet any research method, e.g. soundwalk or lab-based listening test, has its advantages and limitations. The virtual reality (VR) and augmented reality (AR) technology provides an alternative method for soundscape research, which may remain as much context as possible while enable control conditions. The current phase of this research project creates a set of virtual environments, to study the ecological validity of virtual reality environment for soundscape, by comparison of human experiences and responses in a real soundscape and three-dimensional virtual ones. The workflow of scene rendering on different levels of detail is described. The results are made publicly available.

# Structure and Details

### Videos

A recorded video of the scene for each setting. The settings are as follows.

* **AR scene**
based on the recorded scene and artificially added a walking person from another recording.

* **Audio only scene**
no visuals, recording is based on auralisation simulation.

* **Recorded scene**
recorded scene without any special sounds and visuals.

* **VR high resolution scene**
based on 3D virtual geometric model, including auralisation simulation.

* **VR simple scene**
based on 3D virtual geometric model with low resolution and visualisation details, including auralisation simulation.

# Literatur

This data is provided in connection with the following publication.

*Heimes, A., Yang, M., & Vorländer, M. (2021). Virtual reality environments for soundscape research. proceeding of 1st Conference on Sound Perception (CSP).*
